import { Injectable } from '@angular/core';
import { Item } from '../../shared/models/item.model';
import { COLLECTION } from '../collection';

@Injectable()
export class CollectionService {
  private _collection: Item[];

  constructor() {
    this.collection = COLLECTION;
  }

  // get collection
  get collection(): Item[] {
    return this._collection;
  }

  // set collection
  set collection(col: Item[]) {
    this._collection = col;
  }

  // get item by id

  // update item

  // delete item

  // add item

}
